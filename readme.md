# About:

This module provides a simple encryption service to help hide credentials.

The scripts are detailed in the following sections.

## Running scripts:

The scripts are tested on python 3.9.6. Run the scripts in a python virtual environment with the cryptography library
installed:

    pip install cryptography==35.0.0

## encrypt.py

This script can encrypt a file. The output is stored in a .enc file. A key is automatically generated if no key file is
specified. This script will also remove the existing plain text input file.

```
$ python encrypt.py --help
usage: encrypt.py [-h] [--input CREDENTIALS_FILE_PATH] [--key KEY_FILE_PATH] [--out ENCRYPTED_CREDENTIALS_FILE_PATH]

This script will encrypt the credentials file and writes it to a .enc file. Read the readme for more details.

optional arguments:
  -h, --help            show this help message and exit
  --input CREDENTIALS_FILE_PATH
                        path to input file.
  --key KEY_FILE_PATH   path to key file (.pkl). A new one will be generated if nothing is specified or if the file does not exist.
  --out ENCRYPTED_CREDENTIALS_FILE_PATH
                        path to output encrypted credentials file (.enc).
```

The default arguments are:

    CREDENTIALS_FILE_PATH = "./credentials.ini"
    KEY_FILE_PATH = "./key.pkl"
    ENCRYPTED_CREDENTIALS_FILE_PATH = "./credentials.enc"

### Sample usage:

1. Copy the template file provided:

```
   $ cp ./credentials_template.ini ./credentials.ini
```

2. Edit the credentials.ini file.
3. Now you can encrypt the file using:

```
   $ python ./encrypt.py
```

Now the folder will have both the encrypted_credentials file and key file.

You CANNOT decrypt the credentials file without the key file.

Please store the key file in some place safe! If you lose it you’ll no longer be able to decrypt messages; if anyone
else gains access to it, they’ll be able to decrypt all of your credentials, and they’ll also be able to forge arbitrary
credentials that will be authenticated and decrypted.

## decrypt.py

Suppose you want to change the credentials stored. The encrypt.py script deletes the input file, so you cannot edit it
anymore!

The solution is to use decrypt.py. This script can decrypt a .enc file, using the key file.

```
$ python decrypt.py --help
usage: decrypt.py [-h] [--input ENCRYPTED_CREDENTIALS_FILE_PATH] [--key KEY_FILE_PATH] [--out CREDENTIALS_FILE_PATH]

This script will decrypt the credentials file and writes it to a .ini file. Read the readme for more details.

optional arguments:
  -h, --help            show this help message and exit
  --input ENCRYPTED_CREDENTIALS_FILE_PATH
                        path to encrypted credentials file (.enc).
  --key KEY_FILE_PATH   path to key file (.pkl).
  --out CREDENTIALS_FILE_PATH
                        path to output file.
```

The default arguments are:

    ENCRYPTED_CREDENTIALS_FILE_PATH = "./credentials.enc"
    KEY_FILE_PATH = "./key.pkl"
    CREDENTIALS_FILE_PATH = "./credentials.ini"

### Sample usage:

1. Place the credentials.enc file and key.pkl file in this folder.
2. Run decrypt.py, like this:

```
   $ python decrypt.py --input ./credentials.enc --key ./key.pkl 
```

3. Edit the credentials.ini file generated.
4. Run encrypt.py to encrypt the new credentials.

## Using the encrypted files in other applications:

Copy credentials.enc, key.pkl, and decrypt_service.py. Place it in your application.

Then, you can read the config using the following code:

```
from decrypt_service import decrypt_credentials

def get_config(cred_file: Path, key_file: Path) -> configparser.ConfigParser:
   credentials = decrypt_credentials(cred_file, key_file)
   config = configparser.ConfigParser()
   config.read_string(credentials.decode("utf-8"))
   return config

config = get_config(path_to_credentials_file, path_to_key_file)
```

## What to do if you lose the key file:

1. Remove any key.pkl file from this folder.
2. Place the credentials.ini file in this folder.
3. Run encrypt.py again.

```
   $ python ./encrypt.py
```

## What to do if you have messed up the config.ini file:

You will have to enter all the details again:

1. Copy the template file provided:

```
   $ cp ./credentials_template.ini ./credentials.ini
```

2. Edit the credentials.ini file.
3. Now you can encrypt the file using:

```
   $ python ./encrypt.py
```
