import argparse
from pathlib import Path

from decrypt_service import decrypt_credentials

if __name__ == "__main__":
    description = """
    This script will decrypt the credentials file and writes it to a .ini file.
    Read the readme for more details.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input', default=Path("./credentials.enc"), help='path to encrypted credentials file (.enc).',
                        type=Path, dest="encrypted_credentials_file_path")
    parser.add_argument('--key', default=Path("./key.pkl"), help='path to key file (.pkl).', type=Path,
                        dest="key_file_path")
    parser.add_argument('--out', default=Path("./credentials.ini"), help='path to output file.', type=Path,
                        dest="credentials_file_path")
    args = parser.parse_args()

    if not args.key_file_path.exists():
        print(f"Error: key file {args.key_file_path} not found. Cannot decrypt the file.")
        exit(-1)

    if not args.encrypted_credentials_file_path.exists():
        print(f"Error: Encrypted credentials file {args.key_file_path} not found.")
        exit(-1)

    credentials = decrypt_credentials(args.encrypted_credentials_file_path, args.key_file_path)
    with open(args.credentials_file_path, "wb") as _file:
        _file.write(credentials)

    print("Decrypted successfully. Output stored in", args.credentials_file_path)
