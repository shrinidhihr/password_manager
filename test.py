import configparser
from pathlib import Path
from pprint import pprint

from encrypt_service import check_credentials_file, ensure_key_file_exists, encrypt_credentials
from decrypt_service import decrypt_credentials


def get_config(cred_file: Path, key_file: Path) -> configparser.ConfigParser:
    credentials = decrypt_credentials(cred_file, key_file)
    config = configparser.ConfigParser()
    config.read_string(credentials.decode("utf-8"))
    return config


def generate_mock_config():
    config = configparser.ConfigParser()
    config['vms'] = {"vms_user_name": "user_name", "vms_password": "password", "vms_ip": "abc.de.fgh.i"}
    config["notification"] = {"email_sender_user_name": "abc@email.com", "email_sender_password": "password"}
    with open(test_config_file, 'w') as configfile:
        config.write(configfile)


if __name__ == "__main__":
    test_config_file = Path('test.ini')
    test_key_file = Path('test_key.pkl')
    test_encrypted_credentials_file = Path('test_encrypted.enc')

    generate_mock_config()
    check_credentials_file(test_config_file)
    ensure_key_file_exists(Path(test_key_file))
    with open(test_encrypted_credentials_file, "wb") as _file:
        encrypted_credentials =encrypt_credentials(cred_file=test_config_file, key_file=test_key_file)
        _file.write(encrypted_credentials)

    config = get_config(test_encrypted_credentials_file, test_key_file)
    pprint({section: dict(config[section]) for section in config.sections()}, indent=4)

    # Delete the test files
    test_config_file.unlink()
    test_key_file.unlink()
    test_encrypted_credentials_file.unlink()
