import binascii
import configparser
import pickle

from cryptography.fernet import Fernet


def check_credentials_file(credentials_file):
    # To Ensure credentials file is not burgled.
    config = configparser.ConfigParser()
    config.read(credentials_file)
    keys = {
        "vms": [
            "vms_user_name",
            "vms_password",
            "vms_ip"
        ],
        "notification": [
            "email_sender_user_name",
            "email_sender_password"
        ],
        "mongo": [
            'db_name',
            'user_name',
            'password',
            'host',
            'port'
        ]
    }
    for section, key in keys.items():
        assert section in config, f"Please ensure that the credential file has {section} section."
        for k in key:
            assert k in config[
                section], f"Please ensure that the credential file defines the field {k} in {section} section."


def ensure_key_file_exists(file_path):
    if not file_path.exists():
        key = Fernet.generate_key()
        with open(file_path, "wb") as key_file:
            pickle.dump(key, key_file)


def encrypt_credentials(cred_file, key_file):
    with open(key_file, "rb") as key_file:
        key = pickle.load(key_file)
    f = None
    try:
        f = Fernet(key)
    except TypeError as te:
        print(te)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)
    except binascii.Error as be:
        print(be)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)
    except ValueError as ve:
        print(ve)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)
    txt = cred_file.read_bytes()
    return f.encrypt(txt)
