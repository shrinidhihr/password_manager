import argparse
from pathlib import Path

from encrypt_service import check_credentials_file, ensure_key_file_exists, encrypt_credentials

if __name__ == "__main__":
    description = """
    This script will encrypt the credentials file and writes it to a .enc file.
    Read the readme for more details.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input', default=Path("./credentials.ini"), type=Path, dest="credentials_file_path",
                        help='path to input file.')
    parser.add_argument('--key', default=Path("./key.pkl"), type=Path, dest="key_file_path",
                        help='path to key file (.pkl). A new one will be generated if nothing is specified or if the '
                             'file does not exist.')
    parser.add_argument('--out', default=Path("./credentials.enc"), type=Path, dest="encrypted_credentials_file_path",
                        help='path to output encrypted credentials file (.enc).')
    args = parser.parse_args()

    if not args.credentials_file_path.exists():
        print(f"Error: Credentials file f{args.key_file_path} not found.")
        exit(-1)

    check_credentials_file(args.credentials_file_path)
    ensure_key_file_exists(args.key_file_path)
    encrypted = encrypt_credentials(args.credentials_file_path, args.key_file_path)
    with open(args.encrypted_credentials_file_path, "wb") as _file:
        _file.write(encrypted)
    args.credentials_file_path.unlink()
    print("Encrypted successfully.")
    print("Path to encrypted credentials:",args.encrypted_credentials_file_path)
    print("Path to key:",args.key_file_path)
