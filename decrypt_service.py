import binascii
import pickle

from cryptography.fernet import Fernet


def decrypt_credentials(cred_file, key_file):
    with open(key_file, "rb") as key_file:
        key = pickle.load(key_file)
    f = None
    try:
        f = Fernet(key)
    except TypeError as te:
        print(te)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)
    except binascii.Error as be:
        print(be)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)
    except ValueError as ve:
        print(ve)
        print(
            f"Error: Key file has invalid key / is corrupted. Cannot decrypt the file. Please delete the current key "
            f"file and run this script again (without specifying key argument).")
        exit(-1)

    cred_bytes = cred_file.read_bytes()
    return f.decrypt(cred_bytes)
